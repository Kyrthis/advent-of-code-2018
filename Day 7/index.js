console.clear();

const INPUT = "Step G must be finished before step S can begin.\nStep T must be finished before step Q can begin.\nStep A must be finished before step B can begin.\nStep H must be finished before step X can begin.\nStep V must be finished before step O can begin.\nStep Z must be finished before step P can begin.\nStep R must be finished before step J can begin.\nStep L must be finished before step Y can begin.\nStep Y must be finished before step E can begin.\nStep W must be finished before step X can begin.\nStep X must be finished before step B can begin.\nStep K must be finished before step E can begin.\nStep Q must be finished before step P can begin.\nStep U must be finished before step B can begin.\nStep M must be finished before step O can begin.\nStep P must be finished before step N can begin.\nStep I must be finished before step J can begin.\nStep B must be finished before step C can begin.\nStep C must be finished before step O can begin.\nStep J must be finished before step F can begin.\nStep F must be finished before step O can begin.\nStep E must be finished before step D can begin.\nStep D must be finished before step N can begin.\nStep N must be finished before step S can begin.\nStep S must be finished before step O can begin.\nStep W must be finished before step O can begin.\nStep L must be finished before step P can begin.\nStep N must be finished before step O can begin.\nStep T must be finished before step D can begin.\nStep G must be finished before step I can begin.\nStep V must be finished before step X can begin.\nStep B must be finished before step N can begin.\nStep R must be finished before step N can begin.\nStep H must be finished before step J can begin.\nStep B must be finished before step S can begin.\nStep P must be finished before step I can begin.\nStep A must be finished before step J can begin.\nStep A must be finished before step U can begin.\nStep B must be finished before step D can begin.\nStep T must be finished before step A can begin.\nStep U must be finished before step D can begin.\nStep T must be finished before step L can begin.\nStep I must be finished before step E can begin.\nStep R must be finished before step U can begin.\nStep H must be finished before step S can begin.\nStep P must be finished before step F can begin.\nStep Q must be finished before step C can begin.\nStep A must be finished before step P can begin.\nStep X must be finished before step E can begin.\nStep Q must be finished before step N can begin.\nStep E must be finished before step N can begin.\nStep Q must be finished before step O can begin.\nStep J must be finished before step S can begin.\nStep X must be finished before step P can begin.\nStep K must be finished before step U can begin.\nStep F must be finished before step E can begin.\nStep C must be finished before step E can begin.\nStep H must be finished before step K can begin.\nStep W must be finished before step B can begin.\nStep G must be finished before step O can begin.\nStep F must be finished before step N can begin.\nStep I must be finished before step D can begin.\nStep G must be finished before step V can begin.\nStep E must be finished before step S can begin.\nStep Y must be finished before step P can begin.\nStep G must be finished before step E can begin.\nStep P must be finished before step J can begin.\nStep U must be finished before step N can begin.\nStep U must be finished before step F can begin.\nStep X must be finished before step U can begin.\nStep X must be finished before step C can begin.\nStep R must be finished before step Q can begin.\nStep Q must be finished before step E can begin.\nStep Z must be finished before step E can begin.\nStep X must be finished before step F can begin.\nStep J must be finished before step D can begin.\nStep X must be finished before step M can begin.\nStep Y must be finished before step D can begin.\nStep K must be finished before step J can begin.\nStep Z must be finished before step J can begin.\nStep M must be finished before step P can begin.\nStep T must be finished before step M can begin.\nStep F must be finished before step S can begin.\nStep P must be finished before step S can begin.\nStep X must be finished before step I can begin.\nStep U must be finished before step J can begin.\nStep M must be finished before step B can begin.\nStep Q must be finished before step D can begin.\nStep Z must be finished before step I can begin.\nStep D must be finished before step S can begin.\nStep J must be finished before step N can begin.\nStep D must be finished before step O can begin.\nStep T must be finished before step H can begin.\nStep P must be finished before step D can begin.\nStep M must be finished before step F can begin.\nStep Y must be finished before step S can begin.\nStep H must be finished before step I can begin.\nStep Y must be finished before step W can begin.\nStep X must be finished before step J can begin.\nStep L must be finished before step W can begin.\nStep G must be finished before step N can begin.";
// const INPUT = "Step C must be finished before step A can begin.\nStep C must be finished before step F can begin.\nStep A must be finished before step B can begin.\nStep A must be finished before step D can begin.\nStep B must be finished before step E can begin.\nStep D must be finished before step E can begin.\nStep F must be finished before step E can begin.";

// const duration = chr => chr.charCodeAt() - 64; // A == 65. (for full minute, chr-4)
const duration = chr => chr.charCodeAt() - 4; // A == 65. (for full minute, chr-4)

const addStepToGraph = (step, graph) => { //step is a string, graph is {}
	graph[step] = {
		comesAfter: [],
		comesBefore: []
	};
};

const deleteStepFromGraph = (step, graph) => {
	graph[step].comesBefore.forEach(
		subsequent => graph[subsequent].comesAfter.splice(graph[subsequent].comesAfter.indexOf(step), 1)
	);
	delete graph[step];	
};


const createConstraintGraph = (constraints) => {
	const constraintGraph = {};

	constraints.forEach((constraint) => {
		if (!constraintGraph.hasOwnProperty(constraint.antecedent)){
			addStepToGraph(constraint.antecedent, constraintGraph);
		}
		constraintGraph[constraint.antecedent].comesBefore.push(constraint.subsequent);
		
		if (!constraintGraph.hasOwnProperty(constraint.subsequent)){
			addStepToGraph(constraint.subsequent, constraintGraph);
		}
		constraintGraph[constraint.subsequent].comesAfter.push(constraint.antecedent);
	});

	return constraintGraph;
};

const getAvailableSteps = graph => Object.keys(graph).filter(step => graph[step].comesAfter.length === 0 ).sort();

const printHeader = numWorkers => {
	let headerString = "Second\t";
	for (let i=1; i<=numWorkers; i++){
		headerString += `Workr${i}\t`;
	}
	headerString += "Done";
	console.log(headerString);
};

const printWorkers = (time, workers, stepsDone) => {
	let statusString = `${time}\t`;
	for (let i=0; i<workers.length; i++){
		statusString += `${workers[i].step + workers[i].timeFree}\t`;
	}
	statusString += stepsDone.join('');
	console.log(statusString);
};

const day7 = () => {
	console.log(INPUT.split("\n"));
	const constraints = INPUT.split("\n").map(
		str => ({
			antecedent: str.match(/^Step (\w)/)[1],
			subsequent: str.match(/step (\w)/)[1]
		})
	);
	// console.log(constraints);
	
	let constraintGraph = createConstraintGraph(constraints);
	
	let traversalPath = [];
	let availableSteps;
	while ((availableSteps = getAvailableSteps(constraintGraph)).length){
		// console.log("steps:", availableSteps);

		let step = availableSteps[0];
		traversalPath.push(step);
		// console.log("path so far:", traversalPath);
		
		deleteStepFromGraph(step, constraintGraph);
	}
	const puzzle1result = traversalPath.join("");
	
	/* PUZZLE 2 STARTS HERE */

	//reset the input and output
	constraintGraph = createConstraintGraph(constraints);
	let numSteps = Object.keys(constraintGraph).length;
	traversalPath = []; //only reusing this for testing.
			
	// create an array of workers.
	const NUM_WORKERS = 5;
	const workers = new Array(NUM_WORKERS).fill(null).map(x => ({timeFree: 0, step: "."}));
	
	//create a variable to keep track of "current" time (updated every time a step is removed from a worker).
	let time = 0;
	
	printHeader(NUM_WORKERS);
	printWorkers(time, workers, traversalPath);
	while (traversalPath.length < numSteps){
		//iterate over workers[]
		for (let i = 0; i < workers.length; i++){
			//if that worker is free, then ...
			if (workers[i].timeFree <= time){
				//put that worker's step on to the "done" pile if it wasn't idle (.)
				if (workers[i].step !== "."){
					traversalPath.push(workers[i].step);
					// and delete that step from the constraintGraph
					deleteStepFromGraph(workers[i].step, constraintGraph);
					workers[i].step = ".";// make that worker idle
				}
				//pull the next availableSteps, filtering out any that are currently assigned to workers, and put the first one into the space.
				let availableSteps = getAvailableSteps(constraintGraph);
				availableSteps = availableSteps.filter(
					step => workers.every(worker => worker.step !== step) //only keep steps for which it is true that every worker isn't working on it.
				);
				if (availableSteps.length){
					let stepToGiveToWorker = availableSteps.shift();
					workers[i].step = stepToGiveToWorker;
					workers[i].timeFree = time + duration(stepToGiveToWorker);
				}
				// console.log(`${time}\t${workers[0].step+workers[0].timeFree}\t${workers[1].step+workers[1].timeFree}\t${traversalPath.join('')}`);
			}
		}
		printWorkers(time, workers, traversalPath);

		nextFreeTime = workers.reduce((nextFreeTime, worker) => Math.min(nextFreeTime, worker.step === "." ? Infinity : worker.timeFree), Infinity);
		time = nextFreeTime === Infinity ? time : nextFreeTime; //when all workers are idle, the nextFreeTime is Infinity, but we need to preserve the current value of time to assign it to puzzle2result.
	}
	
	const puzzle2result = time;
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}


day7();